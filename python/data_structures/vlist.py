# Basic functionality for link list:
#  - Add (To the end of the list)
#  - Push (To the top of the list)
#  - Pop
#  - get_index
#  - find first occurance
#  - Remove at index
#  - Remove at first occurance
#  - Insert
#  - Length (__len__ override)
#  - print contents (__str__ override)
#  - Detect circular lists?

from vnode import VNode

class VList():

    def __init__(self, data = None):
        if data == None:
            self.head = None
            self.tail = None
            self.__length = 0
        else:
            self.head = VNode(data)
            self.tail = self.head
            self.__length = 1

    def add(self, data):
        if self._init_if_empty(data):
            return
        
        self.tail.add_child(VNode(data))
        self.tail = self.tail.get_child()
        self.__length += 1

    def push(self, data):
        if self._init_if_empty(data):
            return
        new_node = VNode(data)
        new_node.add_child(self.head)
        self.head = new_node
        self.__length += 1

    def pop(self):
        if self.__length == 0:
            raise IndexError("Index out of bounds")
        top_node = self.head
        self.head = top_node.get_child()
        self.__length -= 1
        return top_node.data

    def find_first_occurance(self, target, compare_func = None):
        """
        Arguments:
            compare_func is a function that returns a bool upon comparing two
            inputs
                compare_func(target, unknown)

        """
        compare_func_exists = True
        if compare_func == None:
            compare_func_exists = False
        elif not callable(compare_func):
            raise TypeError("compare_func must be callable with 2 arguments")
            
        current = self.head
        while current != None:
            if compare_func_exists:
                if compare_func(target, current.data):
                    break
            else:
                if (current.data == target):
                    break
            current = current.get_child()

        if current == None:
            return None
        return current.data

    def get_index(self, index):
        if index == 0:
            return self.head
        current = self.head
        while index != 1:
            current = current.get_child()
            index -= 1
        return current.data

    def remove_at_index(self, index):
        if index > self.__length:
            raise IndexError("Index out of bounds")
        previous = None
        current = self.head

        if index == 0:
            self.head = self.head.get_child()
            return

        while index != 1:
            previous = current
            current = current.get_child()
            index -= 1

        self.__length -= 1
        previous.remove_child()
        previous.add_child(current.get_child())
        del current

    def remove_at_first_occurance(self, data, compare_func = None):
        compare_func_exists = True
        if compare_func == None:
            compare_func_exists = False
        elif not callable(compare_func):
            raise TypeError("compare_func must be callable with 2 arguments")

        not_head = False
        previous = None
        current = self.head
        while current != None:
            not_head = True
            if compare_func_exists:
                if compare_func(data, current.data):
                    break
            else:
                if (current.data == data):
                    break
            previous = current
            current = current.get_child()

        if current == None:
            return None

        self.__length -= 1
        if previous != None:
            previous.remove_child()
            previous.add_child(current.get_child())
            del current
        else:
            self.head = self.head.get_child()
            return

        # previous = None
        # current = self.head
        # not_head = False
        # while current.data != data:
        #     not_head = True
        #     previous = current
        #     current = current.get_child()
        #     if current == None:
        #         break

        # if current == None:
        #     return

        # self.__length -= 1
        # if not_head:
        #     previous.remove_child()
        #     previous.add_child(current.get_child())
        #     del current
        # else:
        #     self.head = self.head.get_child()
        #     return

    def _init_if_empty(self, data):
        if self.__length == 0:
            self.head = VNode(data)
            self.tail = self.head
            self.__length = 1
            return True
        return False
    
    def __len__(self):
        return self.__length

    def __str__(self):
        if self.__length == 0:
            return "[]"
        string_of_data = "["
        current = self.head
        while current != None:
            string_of_data += str(current.data) + ','
            current = current.get_child()
        return string_of_data[:len(string_of_data)-1] + ']'

def example():
    vlist = VList()

    vlist.add(7)
    vlist.add(8)
    vlist.add(9)
    vlist.add(10)
    vlist.add(11)
    vlist.add(12)
    vlist.push(13)

    current = vlist.head
    print(vlist)

    print("\nPopped! {0}".format(vlist.pop()))
    current = vlist.head
    print(vlist)

    print("\nRemoving index 3")
    vlist.remove_at_index(3)
    current = vlist.head
    print(vlist)

    print("\nRemoving occurance 10")
    vlist.remove_at_first_occurance(10  )
    print(vlist)

    print("\nRemoving index 0")
    vlist.remove_at_index(0)
    print(vlist)

    print("\nRemoving occurance 8")
    vlist.remove_at_first_occurance(8)
    print(vlist)
