# Basic functionality of a stack 
#  - push onto the stack
#  - pop off of the stack
#  - top 

from vlist import VList

class VStack():

    def __init__(self):
        self.__stack_list = VList()

    def push(self, data):
        self.__stack_list.push(data)

    def pop(self):
        return self.__stack_list.pop()

    def top(self):
        return self.__stack_list.get_index(0)

    def __len__(self):
        return len(self.__stack_list)

    def __str__(self):
        return str(self.__stack_list)


def example():
    vStack = VStack()

    vStack.push(1)
    vStack.push(2)
    vStack.push(3)
    vStack.push(4)
    vStack.push(5)
    vStack.push(6)
    vStack.push(1)

    print(str(vStack))

    print("\nPopping 3 items from stack")
    item1 = vStack.pop()
    item2 = vStack.pop()
    item3 = vStack.pop()
    print("Three data items popped: {0}, {1}, {2}".format(item1, item2, item3))
    print(str(vStack))

    print("\nRemoving all items from stack")
    for x in range(0, len(vStack)):
        print("Data item popped: {}".format(vStack.pop()))
    print(str(vStack))
