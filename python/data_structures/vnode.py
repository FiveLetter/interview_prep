# Basic functionality of Node
#  - Hold data
#  - Reference one other nodes
#  - Add a child

class VNode:

    def __init__(self, data, multiple_children = False):
        self.data = data
        self.multiple_children = multiple_children
        self.__children = []

    def add_child(self, a_node):
        if (len(self.__children) == 1) and not self.multiple_children:
            raise ValueError('Node not configured to handle multiple children!')
        if a_node == None:
            pass
        elif not isinstance(a_node, VNode):
            raise ValueError('Cannot assign a non VNode object as a VNode child')

        self.__children.append(a_node)

    def get_child(self):
        try:
            if self.multiple_children:
                if len(multiple_children) == 0:
                    return None
                return self.__children
            else:
                return self.__children[0]
        except (IndexError):
            return None;

    def remove_child(self):
        self.__children = []
