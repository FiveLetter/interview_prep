# Binary Tree using arrays
#  - insert
#  - remove_first_occurance
#  - search
#  - minimum
#  - maximum
#  - predecssor 
#  - successor
#  - printing
# Notes:
# - Empty nodes will be specified with None type
#
# Array tree example
# 10, 5, 15, 3, 4, 12, 16, 2, -, -, -, 13, -, -, -
#               10
#       5                  15
#  3        4         12        16
# 2  -   -   -     13    -    -    -  
#
# how to find child of a node
# left child  2n+1 
# right child 2n+2
#
# 5 has index of 1 so n = 1
# 2(1) + 1 = 3 => array[3] = 3
# 2(1) + 2 = 4 => array[4] = 4
#
# 12 has index of 5 so n = 5
# 2(5) + 1 = 11 => array[11] = 13
# 2(5) + 2 = 12 => array[12] = - 

class VBiTreeArray():
    def __init__(self):
        self.__tree_array = [None]

    def insert(self, data):
        # If root is empty assign data to root
        if self.__tree_array[0] == None:
            self.__tree_array[0] = data
            self.__add_to_height()
            return

        index = 0
        while True:
            # if data is larger go to right child
            if data > self.__tree_array[index]:
                index = (index * 2) + 2
            # else go to left child
            else:
                index = (index * 2) + 1

            # If index is out of range add more space to tree
            if index > len(self.__tree_array):
                self.__add_to_height()
            # If data at index is None then store data there
            if self.__tree_array[index] == None:
                self.__tree_array[index] = data
                break

    def __add_to_height(self):
        highest_index = len(self.__tree_array) - 1
        new_max = (highest_index * 2) + 2
        while highest_index != new_max:
            self.__tree_array.append(None)
            highest_index += 1

    def remove_first_occurance(self, data):
        index = 0
        while True:
            if self.__tree_array[index] == None:
                break
            if data == self.__tree_array[index]:
                index = (index * 2) + 1
                break
            elif data > self.__tree_array[index]:
                index = (index * 2) + 2
            elif data < self.__tree_array[index]:
                index = (index * 2) + 1

        if index > len(self.__tree_array):
            index = (index - 1)/2
        elif self.__tree_array[index] == None:
            return -1
        print("found value: " + str(self.__tree_array[(index - 1)/2]))
        max_of_branch = self.__maximum(index = index, remove = True)
        print("Maximum of the following branch = {0}".format(max_of_branch))
        self.__tree_array[(index - 1)/2] = max_of_branch

    def search(self, data):
        index = 0
        while True:
            if data == self.__tree_array[index]:
               break
            if data == None:
                return -1
            elif data > self.__tree_array[index]:
                index = (index * 2) + 2
            elif data < self.__tree_array[index]:
                index = (index * 2) + 1 

            if index > len(self.__tree_array):
                return -1
        return index

    def minimum(self):
        return self.__minimum(remove = False)

    def maximum(self):
        return self.__maximum(remove = False)

    def predecessor(self, data):
        """ Fetches the parent of data point
        Args:
            data: data that is being represented within the tree

        Returns:
            The parent data point and index.
                (data, index)
            If the data point passed does not exist in the tree return None
        """
        parent = None
        index = 0
        while True:
            if data == self.__tree_array[index]:
                if (index % 2) == 0:
                    parent = (index - 2)/2
                else:
                    parent = (index - 1)/2
                break
            if data == None:
                break
            elif data > self.__tree_array[index]:
                index = (index * 2) + 2
            elif data < self.__tree_array[index]:
                index = (index * 2) + 1

            if index > len(self.__tree_array):
                break

        if parent == None:
            return None
        return (self.__tree_array[parent], parent)

    def successor(self, data):
        """ Fetches the children of data point
        Args:
            data: data that is being represeted within the tree 

        Returns:
            The children of the data point
                (left child, right child)
        """
        left_child = None
        right_child = None

        if data == self.__tree_array[0]:
            return (self.__tree_array[1], self.__tree_array[2])
        
        index = 1
        while True:
            if data == self.__tree_array[index]:
                left_child = self.__tree_array[index*2 + 1]
                right_child = self.__tree_array[index*2 + 2]
                break
            if data == None:
                break
            elif data > self.__tree_array[index]:
                index = (index * 2) + 2
            elif data < self.__tree_array[index]:
                index = (index * 2) + 1

        if (left_child, right_child) == (None, None):
            return None
        return (left_child, right_child) 

    def __minimum(self, index = 0, remove = False):
        found_value = None
        while True:
            if ((index*2) + 1) > len(self.__tree_array) - 1:
                found_value = self.__tree_array[index]
                if remove:
                    self.__tree_array = None
                break
            elif self.__tree_array[(index*2) + 1] == None:
                found_value = self.__tree_array[index]
                if remove:
                    self.__tree_array[index] = None
                break
            index = (index * 2) + 1

        return self.__tree_array[index]

    def __maximum(self, index = 0, remove = False):
        found_value = None
        while True:
            if ((index*2) + 2) > len(self.__tree_array) - 1:
                found_value = self.__tree_array[index]
                if remove:
                    self.__tree_array[index] = None
                break
            elif self.__tree_array[(index*2) + 2] == None:
                found_value = self.__tree_array[index]
                if remove:
                    self.__tree_array[index] = None
                break
            index = (index * 2) + 2
        
        return found_value

    def __str__(self):
        return (str(self.__tree_array))

def example():
    vbiTreeArray = VBiTreeArray()
    vbiTreeArray.insert(10)
    vbiTreeArray.insert(5)
    vbiTreeArray.insert(15)
    vbiTreeArray.insert(12)
    vbiTreeArray.insert(11)
    vbiTreeArray.insert(3)
    vbiTreeArray.insert(8)
    vbiTreeArray.insert(18)
    vbiTreeArray.insert(19)
    vbiTreeArray.insert(16)
    vbiTreeArray.insert(14)
    vbiTreeArray.insert(2)
    vbiTreeArray.insert(4)
    vbiTreeArray.insert(6)
    vbiTreeArray.insert(9)
    print(vbiTreeArray)

    print("Parent of data point 11: " + str(vbiTreeArray.predecessor(100)))
    print("Children of data point 8: " + str(vbiTreeArray.successor(8)))
    print("minimum: " + str(vbiTreeArray.minimum()))
    print("maximum: " + str(vbiTreeArray.maximum()))

    print("\nRemoving data point 19 from tree:")
    vbiTreeArray.remove_first_occurance(14)
    print(vbiTreeArray)

    print("\nFinding the element 15")
    print("Index of element 15: {0}".format(vbiTreeArray.search(19)))

