# Basic functionality of a queue
#  - add  
#  - peak
#  - remove
#  - is_empty

from vlist import VList 
class VQueue():
    def __init__(self):
        self.__queue_list = VList()

    def add(self, data):
        self.__queue_list.add(data)

    def peak(self):
        return self.__queue_list.get_index(0)

    def remove(self):
        return self.__queue_list.pop()

    def is_empty(self):
        if len(self.__queue_list) == 0:
            return True
        return False

    def __len__(self):
        return (len(self.__queue_list))

    def __str__(self):
        return (str(self.__queue_list))

def example():
    vQueue = VQueue()

    vQueue.add(1)
    vQueue.add(2)
    vQueue.add(3)
    vQueue.add(4)
    vQueue.add(5)
    print(str(vQueue))

    print("\nProcess queue")
    while not vQueue.is_empty():
        print(vQueue.remove())

    print(str(vQueue))


