from vnode import VNode

# Subclass VBinaryTreeNode of vnode
# - left child @ index 0
# - right child @ index 1
# 
# Class variables:
# key //public
# data //public
# parent
# left child
# right child

class VBinaryTreeNode(VNode):
    def __init__ (self, key, data, parent = None):
        
        # This init includes:
        # data
        VNode.__init__(self, data, multiple_children = True)
        self.key = key
        self.__parent = parent
        self._VNode__children.append(None)
        self._VNode__children.append(None)

    def left_child(self, new_node = None, delete = False):
        if delete:
            self._VNode__children[0] = None
            return
        
        if new_node == None:
            return self._VNode__children[0]
        
        if not isinstance(new_node, VBinaryTreeNode):
            raise TypeError("Node must be of type VBinaryTreeNode")
        self._VNode__children[0] = new_node

    def right_child(self, new_node = None, delete = False):
        if delete:
            self._VNode__children[1] = None
            return
        
        if new_node == None:
            return self._VNode__children[1]
        
        if not isinstance(new_node, VBinaryTreeNode):
            raise TypeError("Node must be of type VBinaryTreeNode")
        self._VNode__children[1] = new_node

    def parent(self, new_parent = None):
        if new_parent == None:
            return self.__parent
        
        if not isinstance(new_parent, VBinaryTreeNode):
            raise TypeError("Parent must be of type VBinaryTreeNode")
        self.__parent = new_parent

    def __str__(self):
        return "key: {0:3}   data: {1}".format(self.key, self.data)

def node_example():
    vbitreenode = VBinaryTreeNode(101, 222)
    print(vbitreenode.key)
    print(vbitreenode.data)
    print(vbitreenode.left_child())
    print(vbitreenode.right_child())
    print(vbitreenode.parent())

    vbitreenode.left_child(VBinaryTreeNode(201, 333))
    vbitreenode.right_child(VBinaryTreeNode(202, 444))
    vbitreenode.parent(VBinaryTreeNode(1, 0))

    print(vbitreenode.left_child())
    print(vbitreenode.right_child())
    print(vbitreenode.parent())

# Binary tree using nodes
# - Insert(key, data)
# - Remove(key)
# - In order() => array
# - Pre order() => array
# - Post order() => array
# - Search(key) => Data
# - Min => key, data pair
# - Max => key, data pair
# - Sucessor => key, data pair 
# - Predecessor => key, data pair

class VBinaryTree():
    def __init__(self):
        self.root = None

    def insert(self, key, data):
        previous_node = None
        current_node = self.root
        while current_node != None:
            previous_node = current_node
            if key > current_node.key:
                current_node = current_node.right_child()
            else:
                current_node = current_node.left_child()

        if previous_node == None:
            self.root = VBinaryTreeNode(key, data)
        elif key > previous_node.key:
            previous_node.right_child(VBinaryTreeNode(key, data, parent = previous_node))
        else:
            previous_node.left_child(VBinaryTreeNode(key, data, parent = previous_node))

    def remove(self, key):
        target_node = self._search(key)

        if target_node == -1:
            print("Could not find node with key: {0}".format(key))
            return

        # No children Present
        if target_node.left_child() == None and target_node.right_child() == None:
            if target_node.parent().left_child().key == key:
                target_node.parent().left_child(delete = True)
            else:
                target_node.parent().right_child(delete = True)
            return

        if target_node.left_child() != None and target_node.right_child() != None:
            # both children present
            min_right_tree = self._min(target_node.right_child())
            target_node.key = min_right_tree.key
            target_node.data = min_right_tree.data
            if min_right_tree.key == min_right_tree.parent().left_child().key:
                min_right_tree.parent().left_child(delete = True)
            else:
                min_right_tree.parent().right_child(delete = True)
        elif target_node.left_child() == None:
            # only left child present
            min_right_tree = self._min(target_node.right_child())
            target_node.key = min_right_tree.key
            target_node.data = min_right_tree.data
            min_right_tree.parent().right_child(delete = True)
        elif target_node.right_child() == None:
            # Only right child present
            max_left_tree = self._min(target_node.left_child())
            target_node.key = max_left_tree.key
            target_node.data = max_left_tree.data
            max_left_tree.parent().left_child(delete = True)

    def in_order(self):
        self._in_order(self.root)

    def pre_order(self):
        self._pre_order(self.root)

    def post_order(self):
        self._post_order(self.root)

    def search(self, key):
        found_node = self._search(key)
        if found_node == -1:
            return found_node
        return (found_node.key, found_node.data)

    def min(self):
        min_node = self._min()
        return (min_node.key, min_node.data)

    def max(self):
        max_node = self._max()
        return (max_node.key, max_node.data)

    def predecessor(self, key):
        current_node = self.root
        while current_node != None:
            if current_node.key == key:
                break

            if key > current_node.key:
                current_node = current_node.right_child()
            else:
                current_node = current_node.left_child()

        if current_node == None:
            return -1
        else:
            return current_node.parent()

    def sucessor(self, key):
        target_node = self._search(key)

        if target_node.right_child() != None:
            return self._min(node = target_node.right_child())

    def _in_order(self, node):
        if node != None:
            self._in_order(node.left_child())
            print("{0:3}: {1}".format(node.key, node.data))
            self._in_order(node.right_child())

    def _pre_order(self, node):
        if node != None:
            print("{0:3}: {1}".format(node.key, node.data))
            self._pre_order(node.left_child())
            self._pre_order(node.right_child())

    def _post_order(self, node):
        if node != None:
            self._post_order(node.left_child())
            self._post_order(node.right_child())
            print("{0:3}: {1}".format(node.key, node.data))

    def _search(self, key):
        current_node = self.root
        while current_node != None:
            if current_node.key == key:
                break
            
            if key > current_node.key:
                current_node = current_node.right_child()
            else:
                current_node = current_node.left_child()
        if current_node == None:
            return -1
        else:
            return current_node

    def _min(self, node = None):
        previous_node = None
        current_node = node
        if current_node == None:
            current_node = self.root

        while current_node != None:
            previous_node = current_node
            current_node = current_node.left_child()
        
        return previous_node

    def _max(self, node = None):
        previous_node = None
        current_node = node
        if current_node == None:
            current_node = self.root

        while current_node != None:
            previous_node = current_node
            current_node = current_node.right_child()

        return previous_node

def example():
    vTree = VBinaryTree()
    vTree.insert(50, 111)
    vTree.insert(25, 222)
    vTree.insert(40, 444)
    vTree.insert(75, 757)
    vTree.insert(90, 989)
    vTree.insert(60, 676)
    vTree.insert(51, 111)
    vTree.insert(22, 222)
    vTree.insert(48, 444)
    vTree.insert(72, 757)
    vTree.insert(91, 989)
    vTree.insert(69, 676)

    print("\nIn Order")
    vTree.in_order()

    print("\nPost Order")
    vTree.post_order()

    print("\nPre Order")
    vTree.pre_order()

    print("\nMin value: {0}".format(str(vTree.min())))
    print("Max value: {0}".format(str(vTree.max())))
    print("Search for 72  (-1 for not found): {0}".format(str(vTree.search(72))))
    print("Search for 100 (-1 for not found): {0}".format(str(vTree.search(100))))
    # print("Predecessor for 25: {0}".format(str(vTree.predecessor(25))))
    sucessor = vTree.sucessor(50)
    print("Sucessor = Key: {0} \tData: {1}".format(sucessor.key, sucessor.data))
    # print("Suceessors for 50:\n\tLeft Child: {0}\n\tRight Child: {1}".format( str(sucessors[0]), str(sucessors[1]) ))

    print("\nRemoving node key 60")
    vTree.remove(60)
    print("\nIn Order")
    vTree.in_order()

    print("\nRemoving node key 90")
    vTree.remove(90)
    print("\nIn Order")
    vTree.in_order()

    print("\nRemoving node key 22")
    vTree.remove(22)
    print("\nIn Order")
    vTree.in_order()

    print("\nRemoving node key 72")
    vTree.remove(72)
    print("\nIn Order")
    vTree.in_order()

    print("\nRemoving node key 69")
    vTree.remove(69)
    print("\nIn Order")
    vTree.in_order()