# Hash map will be a bucket hash map (linked lists)
# Basic functionality for Hash Map:
#  - Insert
#  - Remove
#  - Get
#  - Hash function
#
# Each node is a tuple (key, data)
from vlist import VList

class VHashMap():
    def __init__(self):
        self.__table = [VList() for x in range(20)]

    def insert(self, key, value):
        index = self._hash(key)
        result = self.__table[index].find_first_occurance(key, 
                                    compare_func = self.__compare_func)
        # If key exists then raise an error
        if result != None:
            raise ValueError("key already exists")

        self.__table[index].add((key, value))

    def remove(self, key):
        index = self._hash(key)
        self.__table[index].remove_at_first_occurance(key, 
                            compare_func = self.__compare_func)

    def get(self, key):
        index = self._hash(key)
        return self.__table[index].find_first_occurance(key, 
                                    compare_func = self.__compare_func)

    def __compare_func(self, key, unknown):
            if key == unknown[0]:
                return True
            return False

    def _hash(self, key):
        """
        All keys must be strings and will be calculated as such
        This is a random equation I cooked up... so ya... thats that
        """
        if not isinstance(key, str):
            raise ValueError("key must be a string")
        return (ord(key[0]) * len(key))%len(self.__table)

    def __str__(self):
        complete_string = ""
        for index in range(20):
            complete_string += "{0}: {1}\n".format(index, self.__table[index])
        return complete_string[:len(complete_string) - 1]

def example():
    vhashmap = VHashMap()

    vhashmap.insert("papi", 111333)
    vhashmap.insert("key", 10292)
    vhashmap.insert("mar", 111333)
    vhashmap.insert("dar", 10292)
    vhashmap.insert("har", 111333)
    vhashmap.insert("bar", 10292)
    vhashmap.insert("dear", 111333)
    vhashmap.insert("deer", 10292)
    vhashmap.insert("li", 111333)
    vhashmap.insert("pos", 10292)
    vhashmap.insert("ls", 111333)
    vhashmap.insert("dir", 10292)
    vhashmap.insert("make", 111333)
    vhashmap.insert("crap", 10292)
    print(str(vhashmap))

    print("\nFetching element with key ls")
    print(vhashmap.get("ls"))

    print("\nRemoving element with key ls")
    vhashmap.remove("ls")
    print(str(vhashmap))

    print("\nRemoving element with key li")
    vhashmap.remove("li")
    print(str(vhashmap))