import sys
from data_structures import *

options = {
    0: (vlist.example, "VList example"),
    1: (vstack.example, "VStack example"),
    2: (vqueue.example, "VQueue example"),
    3: (vbitreearray.example, "VBitTreeArray example"),
    4: (vhashmap.example, "VHashMap example"),
    5: (vbinarytree.example, "VBinaryTree example")
}

while True:
    for key in options.keys():
        print("(" + str(key) + ") "+ options[key][1])
    option = raw_input("Execute option (q to quit): ")
    if option == "":
        print("")
        continue
    if option == 'q':
        sys.exit(0)
    try:
        option = int(option)
        if option not in options.keys():
            print("available keys: ")
            print(options.keys())
            print("\n{0} is an invalid option.".format(option))
        else:
            print("+++++++++++++++ EXECUTION +++++++++++++++")
            options[option][0]()
            print("++++++++++++ DONE EXECUTION +++++++++++++\n")
    except ValueError:
        print("{0} is not a valid key".format(option))