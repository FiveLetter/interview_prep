import turtle
import time

def init_window():
    window = turtle.Screen()
    window.bgcolor("red")
    brad = turtle.Turtle()
    brad.shape('turtle')
    brad.color('yellow')
    brad.speed(8)

    for i in range(0, 100):
        draw_square(brad)
        brad.right(10)
        
    window.exitonclick()
    
def draw_square(a_turtle):
    for x in range(0,4):
        a_turtle.forward(100)
        a_turtle.right(90)

def draw_circle():
    angie = turtle.Turtle()
    angie.shape("arrow")
    angie.circle(100)
    angie.color('blue')


init_window()

