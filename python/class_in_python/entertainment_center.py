import media

toy_story = media.Movie("Toy Story", "A story of a boy and his toys", 
                        "empty URI", "empty URL")
print(toy_story.storyline)


avatar = media.Movie("Avatar", "It's about blue people",
                     "http://upload.wikimedia.org/wikipedia/id/b/b0/Avatar-Teaser-Poster.jpg",
                     "http://www.youtube.com/watch?v=-9ceBgWV8io")