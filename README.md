# **Interview Prep**
----
This repository is meant for personal use to get ready for interviews. Each language will have its own directory. 


Remember that it is okay to ask questions to interviewers for clarifications. Do not assume anything, instead ask your interviewer if its a correct assumption. As you program verbally explain how you are approaching each part of the question. ALWAYS LISTEN!

**Tips from cousins**


* Make a working solution first, if you have time then create or describe a more optimal solution. 


**Basic principles for any interview**


* data structures
* algorithms
* system design
* big O notation (Algorithm Complexity)

**Data Structures**


* Link List
* Hash Tables, explain and code one
* Binary trees: tree construction, traversal, manipulation
* n-ary trees: tree construction, traversal, manipulation
* trie-trees: tree construction, traversal, manipulation
* red/black tree, a splay tree, or AVL tree
* Tree Traversal, BFS and DFS, inorder, postoder, preorder, computational complexity, tradeoffs, and implementation
* stacks
* Queues
* Graphs, three basic ways to represent a graph: objects & pointers, matrix, adjacency list. Know pros and cons

**Concepts**


* big O notation (Algorithm Complexity)
* Sorting, don't do bubble-sort. Known at least one n*log(n): quicksort or merge sort
* Dijkstra and A*
* Traveling Salesman/Knapsack
* OS: processes, threads, concurrency, locks, mutexes, semaphores, monitors, deadlocks, context switching
* Recursion and induction
* System design


## **Python**
-----
**Topics ACTUALLY covered:**
If Time permits change functions to recursive versions

* Linked list
* Stack
* Queue
* Binary Tree using Arrays
* Binary Tree using Nodes
* Hash Maps: Bucket style



## **Useful Links**
-----
https://sites.google.com/site/steveyegge2/five-essential-phone-screen-questions


http://research.google.com/archive/gfs.html


https://leetcode.com/


https://docs.google.com/document/d/1Rl7R7VhKpiSB0qHuvJiDedJZocx0aNdMPYqpZ9A5Puk/edit#